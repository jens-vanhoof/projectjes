
 <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="memberModalLabel">Welkom op mijn site!</h4>
      </div>
      <div class="modal-body">
        <p>Beste bezoeker van mijn site, mag ik u van harte verwelkomen op mijn site.
        Ik hoop dat ik u alvast een goede indruk heb kunnen geven toen u deze site opende.
        Om u niet op een onbekende persoon zijn site te laten ronddwalen stel ik mezelf even voor.</p>

        <p>Ik ben Jens Van Loon, een leerling van het Immaculata Instituut te Oostmalle.
        Geboren in het AZ Middelheim in Antwerpen sta ik al 17 jaar op deze wereld. In deze 17 jaren heb ik 13 jaren gedanst.
        Met driemaal de titel ‘Belgisch Kampioen’ kan ik toch met trots zeggen dat ik al wat bereikt heb in mijn leven.
        Naast dansen is informatica ook al altijd een belangrijk deel geweest in mijn leven.
        Op mijn 3 jaar kon ik al beter overweg met computers dan mijn ouders.
        Nu bent u hier, bezoeker op mijn website, de website van een zesdejaars in Informaticabeheer.</p>

        <p>Mijn hoofddoel voor deze site was het strak design, iets dat dus ook makkelijk te gebruiken is.
        Ik heb gekozen om de hoofdzaken makkelijk beschikbaar te stellen.
        Zo kan u al mijn taken gewoon terugvinden op de hoofdpagina, door op een hiervan te klikken opent hij direct en kan u rustig lezen.
        U kan steeds teruggaan via het “HOME” knopje in de navigatiebar, die ook op elke pagina aanwezig is.
        Op mobiele apparaten heb ik gekozen dit weg te laten en de onderdelen enkel via het menu aan de zijkant te laten zien.
        Als u op de knop “POSTS” klikt, vindt u een duidelijk overzicht van alle taken die doorheen het jaar gemaakt worden.
        Via de knop “STAGEVERSLAGEN” komt u bij mijn stageverslagen terecht die u kan opvolgen van 11 januari tot 22 januari.
        Verder is het gebruik van de site dat voor zichzelf spreekt.</p>

        <p>Hierbij hoop ik u voldoende te hebben ingeleid op mijn site.
        Wat u verder van mij kan verwachten? Ik ben een gedreven persoon en als ik iets wil bereiken zal ik hiertoe hard werken.
        In de IT-wereld is de definitie hard werken letterlijk van toepassing en is het geen geluk hebben.
        Ik zou ten eerste zeker een master willen krijgen in de toegepaste informatica.
        Na wat research te hebben gedaan kwam ik erachter dat Google opleidingen aanbied aan leerlingen met een master-diploma.
        Dit is dan ook mijn grote wens, om dan nadien in Californië aan de slag te kunnen gaan.
        Wie weet hoort u later nog van mij op een veel grotere schaal, dat is alvast waarnaar ik streef.</p>

        <p>Hierbij laat ik u volledig los op mijn site.
        U kan rustig mijn posts lezen of mijn stageverslagen.
        Indien u mij wilt contacteren kan dit steeds via <a class="link" href="mailto:jens.vanloon@immalle.be?SUBJECT=Contact via site&BODY=Uw vraag hier ...">jens.vanloon@immalle.be</a>.
        Alvast veel leesplezier gewenst!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Sluit</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#modal').click(function () {

  $('#memberModal').modal('show');

});

  </script>
[22:39:03] Jens : <script type="text/javascript">
$('#modal').click(function () {

  $('#memberModal').modal('show');

});

  </script>